<?xml version="1.0" encoding="utf-8"?>
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".vue.fragment.AccueilFragment">

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <ScrollView
            android:id="@+id/interfaceScrollView"
            android:layout_width="match_parent"
            android:layout_height="600dp"
            android:padding="15sp"
            android:scrollbarSize="5sp"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintHorizontal_bias="0.0"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintVertical_bias="0.496">

            <LinearLayout
                android:id="@+id/registrationScrollViewLinearLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <androidx.cardview.widget.CardView
                    android:id="@+id/interfaceCardView"
                    android:layout_width="match_parent"
                    android:layout_height="400dp"
                    android:layout_marginBottom="20dp"
                    app:cardBackgroundColor="#226D68"
                    app:cardCornerRadius="25sp"
                    app:cardElevation="50sp"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent">

                    <ImageView
                        android:id="@+id/interfaceImageView"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:adjustViewBounds="true"
                        android:scaleType="centerInside"
                        android:src="@drawable/picture"
                        android:layout_gravity="center"
                        android:layout_marginLeft="10dp"
                        android:layout_marginTop="10dp"
                        android:layout_marginBottom="30dp"
                        android:layout_marginRight="10dp"/>

                    <TextView
                        android:id="@+id/enregistreImage"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_gravity="bottom"
                        android:layout_marginStart="20dp"
                        android:layout_marginBottom="10dp"
                        android:text="Enregistre"
                        android:textColor="@color/white_share_it"
                        android:textSize="20sp"
                        android:textStyle="bold"/>

                </androidx.cardview.widget.CardView>
                <Button
                    android:id="@+id/buttonPhoto"
                    android:layout_width="150dp"
                    android:layout_height="150dp"
                    android:layout_gravity="center"
                    android:text="@string/prendre_une_photo"
                    android:background="@drawable/boutonrond"/>
            </LinearLayout>
        </ScrollView>
    </androidx.constraintlayout.widget.ConstraintLayout>
</FrameLayout>