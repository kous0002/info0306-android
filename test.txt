 <!--android:name="fr.univ_reims.informatique.islam.shareit.vue.fragment.AccueilFragment"-->
-------------------------------------------------------------------------------------------------

    /**
     * initialisation de l'activities
     */
    /*
    private void initialisationActivity(){
        //recup des objet
        buttonFloating =  findViewById(R.id.buttonfloating);
        imgView = findViewById(R.id.interfaceImageView);
        enregistreBoutton = findViewById(R.id.enregistreImage);
        //boutton pour gerer les évenements
        createOnClickFloatingActionButton();
        createOnClickEnregistreImage();
    }
     */
    /**
     * evenement enregistre image
     */
    /*
    private void createOnClickEnregistreImage(){
        enregistreBoutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ENREGISTRE LA PHOTO
                MediaStore.Images.Media.insertImage(getContentResolver(),image,"img","img");
            }
        });
    }
*/
    /**
     * Evenement clique sur le floating button
     */
   /*
    private void createOnClickFloatingActionButton() {
        buttonFloating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prendrePhoto();
            }
        });
    }
*/
    /**
     *  Acces a l'appareil photo et stocker dans un fichier tamporaire
     */
   /*
    private void prendrePhoto(){
        // creation d'un intent pour la fenetre pour prendre la photo
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //startActivityForResult(intent,1);
        //créer un nom de fichier unique
        //pour cela on recupere la date et l'heure
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //le chemin externe de l'environnement
        File photoDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        try {
            //creation du fichier
            File photoFile = File.createTempFile("photo"+time, ".jpg",photoDir);
            //enregistrer le chemin absolue
            photoPath = photoFile.getAbsolutePath();
            //utilisation du provider pour crée l'uri
            Uri photoUri = FileProvider.getUriForFile(InterfaceActivity.this, InterfaceActivity.this.getApplicationContext()+".provider",photoFile);
            //transfere uri vers l'intent pour enregistrer la photo dans le fichier temporaire
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            //ouvrir activity par rapport a l'intent
            startActivityForResult(intent,RETOUR_PHOTO);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
*/
    /**
     * Retour de l'appel de l'appareil photo
     * @param requestCode
     * @param resultCode
     * @param data
     */
  /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data ) {
        super.onActivityResult(requestCode, resultCode, data);
        //vérifier le bon fonctionnement du code de retour et l'état du retour ok
        if(requestCode==RETOUR_PHOTO && resultCode==RESULT_OK){
            //récupérer l'image
            image = BitmapFactory.decodeFile(photoPath);
            //afficher l'image
            imgView.setImageBitmap(image);
        }

    }
    */